import './style.css';

function Header(){
    var header_style={
        boxShadow: "1px 1px 10px 1px black"
    }

    return(
        <div className="header" style={header_style}>
            
            <div className="logotipo">
                <h3>Théo Furtado</h3>
            </div>

            <div className="menu-header">
                <ul>
                    <li>Home</li>
                    <li>Sobre</li>
                    <li>Experiencia</li>
                    <li>Projetos</li>
                    <li>Contato</li>
                </ul>
            </div>
            
        </div>

    )
}



export default Header;