import './style.css';

function Sobre(){
    return(
        <div className="sobre">
            <div className="container">
                <div className="box-conteudo">
                    <h3>SOBRE MIM</h3>
                    <p>Olá, eu sou Théo Furtado, tenho 20 anos,
                        moro no Rio de Janeiro. Meus amigos me consideram
                        uma pessoa extremamente focada em meus
                        sonhos e objetivos, adoro passar meu tempo
                        livre com a minha familia e amigos. Adoro
                        assistir jogos do meu time no estádio, mas 
                        quando não dá, topo e me amarro em um 
                        barzinho. Minha paixão por tecnologia começou
                        bem novo, com a influencia do meu tio, que se formou
                        em ciencia da computação e sempre instalava jogos
                        para mim no computador de tubo que minha avó 
                        tinha. Em 2015 decidi aprender a programar para
                        desenvolver jogos e aplicativos, e hoje estou
                        graduando na area que sempre amei.
                    </p>


                </div>
                <div className="box-imagem">
                    <div className="bg-img">
                        {/*ADICIONAR FOTO MINHA SEM BG*/}
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Sobre;