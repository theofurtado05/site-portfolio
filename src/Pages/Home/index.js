
import Header from '../../Componentes/home/header';
import Sobre from '../../Componentes/home/sobre';
import Atividades from '../../Componentes/home/atividades';
import Projetos from '../../Componentes/home/projetos';
import Contato from '../../Componentes/home/contato';
import Experiencias from '../../Componentes/home/experiencias';


export default function App(){
    return (
        <div>
            <Header />

            <Sobre />
        
            <Atividades />

            <Experiencias />

            <Projetos />

            <Contato />

        </div>

    )
}

